#ifndef EMPRESA_H
#define	EMPRESA_H

#include <vector>
#include "Fijo.h"
#include "Empleado.h"
#include "Jornalero.h"
#include <typeinfo>
using namespace std;

class Empresa:public Empleado {
protected:
    string nombre;
    string nombreLegal;
    int rut;
    vector<Empleado*> emp;
    
public:
    Empresa();
    Empresa(const Empresa& orig);
    virtual ~Empresa();
    Paga total_sueldo_peso();
    Paga total_sueldo_dolar();
    void setNombre(string);
    void setNombreLegal(string);
    void setRut(int);
    string getNombre();
    string getNombreLegal();
    vector<Empleado*> getEmp();
    int getRut();
    void insertEmpleado(Empleado* e);
};

#endif	/* EMPRESA_H */

