#include "Jornalero.h"

Jornalero::Jornalero() {
}

Jornalero::Jornalero(string nombre, string ci, int edad, Paga valor_hora,Empresa *e, int h) {
    Empleado::setNombre(nombre);
    Empleado::setCi(ci);
    Empleado::setEdad(edad);
    Empleado::setValor_hora(valor_hora);
    Empleado::setEmpresa(e);
    this->horas = h;
}


Jornalero::~Jornalero() {
}

int Jornalero::getHoras() {
    return horas;
}

void Jornalero::setHoras(int h) {
    horas=h;
}

Paga Jornalero::get_sueldo_dolar() {
    return getValor_hora().a_dolar()*horas;
}

Paga Jornalero::get_sueldo_peso() {
    return getValor_hora().a_peso()*horas;
}

