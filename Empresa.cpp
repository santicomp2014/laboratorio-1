#include "Empresa.h"
#include "Menu.h"
#include "Jornalero.h"

Empresa::Empresa() {
}

Empresa::Empresa(const Empresa& orig) {
}

Empresa::~Empresa() {
}
vector<Empleado*> Empresa::getEmp(){
    return emp;
}
void Empresa::setNombre(string n) {
    nombre = n;
}

void Empresa::setNombreLegal(string n) {
    nombreLegal = n;
}

void Empresa::setRut(int r) {
    rut = r;
}

string Empresa::getNombre() {
    return nombre;
}

string Empresa::getNombreLegal() {
    return nombreLegal;
}

int Empresa::getRut() {
    return rut;
}

Paga Empresa::total_sueldo_peso() {
    Paga total(0, US);
    for (int i = 0; i < emp.size(); i++) {
        total += emp[i]->get_sueldo_peso();
    }
    return total;
}

Paga Empresa::total_sueldo_dolar() {
    Paga total(0, USD);
    for (int i = 0; i < emp.size(); i++) {
        total += emp[i]->get_sueldo_dolar();
    }
    return total;
}
void Empresa::insertEmpleado(Empleado* e){
    emp.push_back(e);
}
