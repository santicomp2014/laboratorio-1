#include "Paga.h"
#include "Menu.h"

Paga::Paga() {
    monto = 0;
    moneda = US;
}

Paga::Paga(double f, Moneda m) {
    this->monto = f;
    this->moneda = m;
}

Paga::~Paga() {
}

Paga Paga::a_dolar() {
    if (moneda == US) {
        return Paga(monto / DOLAR, USD);
    }
    return Paga(monto, moneda);
}

Paga Paga::a_peso() {
    if (moneda == USD) {
        return Paga(monto * DOLAR, US);
    }
    return Paga(monto, moneda);
}

void Paga::setMonto(double m) {
    monto = m;
}

double Paga::getMonto() {
    return this->monto;
}

void Paga::setMoneda(Moneda m) {
    moneda = m;
}

Moneda Paga::getMoneda() {
    return this->moneda;
}

Paga Paga::operator*(int horas) {
    if (horas < 0)
        throw 3;
    return Paga(monto*horas, moneda);
}

void Paga::operator+=(Paga p) {
    monto += p.getMonto();
}

Paga Paga::operator+(Paga p) {
    if (moneda != p.getMoneda()) {

        throw 2;
    }
    return Paga(monto + p.getMonto(), moneda);
}



ostream& operator<<(ostream& x, Paga p) {
    double a = p.getMonto();
    double aux = a;
    int digitos = 1;
    while (aux >= 10) {
        aux = aux / 10;
        digitos++;
    }
    while (digitos >= 1) {
        double elevacion = 1;
        for (int i = 3; (digitos - 1) >= i; i = i + 3) {
            elevacion = elevacion * 1000;
        }
        int tresDigitos = (int) (a / elevacion);
        if (p.monto / elevacion < 1000) {
            if (tresDigitos == 0) {
                x << "0";
            } else {
                x << tresDigitos;
            }
        } else {
            if (tresDigitos >= 100) {
                x << "." << tresDigitos;
            } else if (tresDigitos >= 10) {
                x << ".0" << tresDigitos;
            } else if (tresDigitos >= 1) {
                x << ".00" << tresDigitos;
            } else {
                x << ".000";
            }
        }
        a = a - ((int) (a / elevacion)) * elevacion;
        digitos = digitos - 3;
    }
    int centavos = (int) (a * 100);
    if (centavos >= 10) {
        x << "," << centavos;
    } else if (centavos >= 1) {
        x << ",0" << centavos;
    } else {
        x << ",00";
    }
    if (p.moneda == US) {
        x << " US";
    } else {
        x << " USD";
    }
}

