/* 
 * File:   Menu.h
 * Author: nahuel
 *
 * Created on 22 de abril de 2015, 11:36 AM
 */

#ifndef MENU_H
#define	MENU_H
#include <iostream>
#include <cstdlib>
#include "Empresa.h"
using namespace std;
class Menu {
private:
    Empresa *e;
public:
    Menu(Empresa*);
    void initMenu();
    void imprimo( );
    void Menu_convertir( );
    void Menu_empleado( );
    void Menu_sobrecarga( );
    void Menu_total_sueldo();
    void elegirSobrecarga(int ,Paga);
    Menu(const Menu& orig);
    Moneda elegirMoneda(Paga);
    virtual ~Menu();
    void Mostrar();
    void eliminar_emp();
    void deleteEmp(string );
    void printList();
    int count_emp();
    void addEmp();
private:
    
};

#endif	/* MENU_H */

