/* 
 * File:   Menu.cpp
 * Author: nahuel
 * 
 * Created on 22 de abril de 2015, 11:36 AM
 */

#include "Menu.h"

#include <iostream>
using namespace std;

Menu::Menu(Empresa * e) {
    this->e=e;
  
}

void Menu::imprimo() {
    cout << endl << "Menu" << endl << "[1]-  Operaciones de sobrecarga"
            << endl << "[2]-  Operaciones para Empleado"
            << endl << "[3]-  Operaciones de cambio de moneda "
            << endl << "[4]-  Total del sueldo de los empleados "
            << endl << "[5]-  salir" << endl;

}

void Menu::Menu_convertir() {
    double monto;
    int op;
    cout << "[1] Convertir  dolares a pesos" << endl
            << "[2] Convertir  pesos a dolares" << endl;
    try {
        cin >> op;
        if (op > 2 || op < 0) throw 1;
        if (op == 1) {

            cout << "Ingrese el monto a convertir" << endl;
            cin >> monto;
            if (monto < 0) throw 2;
            cout << "El resultado en pesos es , " << Cambio::a_peso(monto) << " US";
        }
        if (op == 2) {
            cout << "Ingrese el monto a convertir" << endl;
            cin >> monto;
            if (monto < 0) throw 2;
            cout << "El resultado en dolares es , " << Cambio::a_dolar(monto) << " USD";
        }
    } catch (int error) {
        if (error == 1) cout << "ERROR ,parametros invalidos";
        if (error == 2) cout << "ERROR ,no se admiten parametros negativos";
    }


}

void Menu::Menu_empleado() {
    cout << "[1] Agregar empleado a la lista" << endl
            << "[2] Modificar datos del empleado" << endl
            << "[3] Eliminar empleado de la lista" << endl
            << "[4] Mostrar lista de empleados" << endl;
}

void Menu::Menu_sobrecarga() {
            cout<< "[1] Operacion +" << endl
            << "[2] Operacion *" << endl
            << "[3] Operador <<" << endl;
}

void Menu::Menu_total_sueldo() {
    cout << "[1] Total sueldo en pesos :" << endl
            << "[2] Total sueldo en dolares :" << endl;
    int op;
    cin >> op;
    try {
        if (op > 2 || op < 0)throw 1;
        if (op == 1) {
            cout << e->total_sueldo_peso();
        }
        if (op == 2)cout << e->total_sueldo_dolar();
    } catch (int error) {
        if (error == 1)cout << "ERROR, parametros invalidos";
    }

}

Menu::Menu(const Menu& orig) {
}

Menu::~Menu() {
}

Moneda Menu::elegirMoneda(Paga m) {

}

void Menu::Mostrar() {
    Menu::printList();
}

void Menu::eliminar_emp() {

    string ci;
    cout << "Ingresar ci del empleado a eliminar" << endl;
    cin >> ci;
    //    e->deleteEmp(ci);
}
void Menu::initMenu(){
 Paga p(0,US);
    int op, op1;
    do {
        imprimo();
        cin>>op;
        //system("clear");
        try {

            if (op < 1 || op > 5) throw 1;
        } catch (int error) {
            if (error == 1) cout << "ERROR, parametros validos [1-5]"
                    << " intente con otro." << endl;
        }
        switch (op) {
            case 1:
                Menu_sobrecarga();
                cin>>op1;
                try {

                    if (op1 < 1 || op1 > 3)throw 1;
                    
                    elegirSobrecarga(op1, p);
                } catch (int error) {
                    system("clear");
                    if (error == 1)cout << " ERROR, no es un parametro valido ";
                    if (error == 2)cout << " ERROR, el tipo de moneda no es el mismo";
                    if (error == 3)cout << " ERROR, no se aceptan parametros negativos";

                }
                break;
            case 2:
                system("clear");
                Menu_empleado();

                cin>>op1;
                try {
                    system("clear");   
                    if (op1 < 1 || op1 > 4)throw 1;
                   
                    if (op1 == 1)addEmp();
                    if (op1 == 3) {
                     //   M->eliminar_emp(empresa);
                    }
                     
                    if (op1 == 4) {
                        system("clear");
                        Mostrar();
                    }
                } catch (int error) {
                    if (error == 1)cout << " ERROR, no es un parametro valido ";
                    if (error == 2)cout << " ERROR,la lista esta llena ";
                    if (error == 3)cout << " ERROR,la lista esta vacia ";
                    if (error == 4)cout << " ERROR,No se admiten parametros negativos ";

                }
                break;
            case 3:
                system("clear");
                Menu_convertir();
                break;
            case 4:
                system("clear");
                Menu_total_sueldo();
                break;
            default:
                throw 1;
        }
    } while (op != 5);
}

void Menu::elegirSobrecarga(int op, Paga p) {
    double v;
    int op1;
    if (op == 1) {
        cout << "elegir monto a asignar";
        cin>> v;
        p.setMonto(v);
        if (p.getMoneda() == 0) {
            cout << " US";
        } else {
            cout << " USD";
        }
    }
    if (op == 2) {
        Paga aux;
        Paga resultado;
        cout << "Monto = " << endl;
        cin>> v;
        if (v < 0) {
            throw 1;
        }
        p.setMonto(v);
        cout << "tipo de moneda " << endl
                << "[1] US " << endl << "[2] USD " << endl;
        cin>>op1;
        switch (op1) {
            case 1:
                p.setMoneda(US);
                break;
            case 2:
                p.setMoneda(USD);
                break;
            default:
                throw 1;
        }
        cout << p.getMoneda();
        cout << "Monto = " << endl;
        cin>> v;
        if (v < 0) {
            throw 1;
        }
        aux.setMonto(v);
        int op1;
        cout << "tipo de moneda " << endl
                << "[1] US " << endl << "[2] USD " << endl;
        cin>>op;
        switch (op) {
            case 1:
                aux.setMoneda(US);
                break;
            case 2:
                aux.setMoneda(USD);
                break;
            default:
                throw 1;
        }
        if (aux.getMoneda() == p.getMoneda()) {
            resultado.setMoneda(p.getMoneda());

        }
        cout << resultado.operator+(p + aux);

    }
    if (op == 3) {
        int cant;
        cout << "Monto = " << endl;
        cin>> v;
        if (v < 0) throw 3;
        p.setMonto(v);
        cout << "tipo de moneda " << endl
                << "[1] US " << endl << "[2] USD " << endl;
        cin>>op;
        switch (op) {
            case 1:
                p.setMoneda(US);
                break;
            case 2:
                p.setMoneda(USD);
                break;
            default:
                throw 1;
        }
        cout << "Cantidad de horas = ";
        cin>>cant;
        if (cant < 0)throw 3;
        cout << p.operator*(cant);
    }
    if (op == 4) {
        cout << "Monto ";
        cin >>v;
        if (v < 0) throw 3;
        p.setMonto(v);
        cout << "tipo de moneda " << endl
                << "[1] US " << endl << "[2] USD " << endl;
        cin>>op;
        switch (op) {
            case 1:
                p.setMoneda(US);
                break;
            case 2:
                p.setMoneda(USD);
                break;
            default:
                throw 1;
        }

        cout << p;

    }
}
void Menu::addEmp() {

    int op, edad, m;
    double valorHora;
    string nombre, cedula;
    Paga p(0, US);
    cout << "El empleado a agregar, va a ser jornalero o fijo ? " << endl
            << "[1] Fijo " << endl
            << "[2] Jornalero " << endl;
    cin >> op;

    if (op > 2 || op < 0) throw 1;

    if (op == 1) {
        cout << "Nombre" << endl;
        cin >> nombre;
        cout << "Cedula" << endl;
        cin >> cedula;
        cout << "Edad " << endl;
        cin >> edad;
        cout << "Valor de la hora ";
        cin >> valorHora;
        p.setMonto(valorHora);
        cout << "Tipo de moneda " << endl << "[1] US" << endl << "[2] USD" << endl;
        cin >> m;
        if (m > 2 || m < 0) throw 1;
        if (m == 1)p.setMoneda(US);
        if (m == 2)p.setMoneda(USD);

        Empleado* empleado = new Fijo(nombre, cedula, edad, p, e);
        e->insertEmpleado(empleado);

    } else {
        int horas;
        cout << "Nombre" << endl;
        cin >> nombre;
        cout << "Cedula" << endl;
        cin >> cedula;
        cout << "Edad " << endl;
        cin >> edad;
        cout << "Valor de la hora " << endl;
        cin >> valorHora;
        p.setMonto(valorHora);
        cout << "Tipo de moneda " << endl << "[1] US" << endl << "[2] USD" << endl;
        cin >> m;
        if (m > 2 || m < 0) throw 1;
        if (m == 1)p.setMoneda(US);
        if (m == 2)p.setMoneda(USD);
        cout << "cantidad de horas" << endl;
        cin >> horas;

        Empleado* empleado = new Jornalero(nombre, cedula, edad, p, e, horas);
        e->insertEmpleado(empleado);
    }
}

void Menu::printList() {

    if (e->getEmp().empty() == true) {
        throw 3;
    }
    //cout << "Nombre" << " " << "cedula " << " " << "edad " << " " << "valor hora " << endl;
    for (int i = 0; i < e->getEmp().size(); i++) {

        if (typeid (*e->getEmp().at(i)) == typeid (Jornalero)) {
            Jornalero * aux = dynamic_cast<Jornalero*> (e->getEmp()[i]);
            //aux = dynamic_cast<Empleado*> (emp[i]);


            cout << aux->getNombre() << " ";
            cout << aux->getCi() << " ";
            cout << aux->getEdad() << " ";
            cout << aux->getValor_hora() << " ";
            cout << aux->getHoras();



        }
        if (typeid (*e->getEmp().at(i)) == typeid (Fijo)) {
            Fijo * aux = dynamic_cast<Fijo*> (e->getEmp()[i]);
            cout << aux->getNombre() << " ";
            cout << aux->getCi() << " ";
            cout << aux->getEdad() << " ";
            cout << aux->getValor_hora() << endl;


        }


    }

}

void Menu::deleteEmp(string cedula) {
    if (e->getEmp().empty() == true) {
        throw 3;
    }
    for (int i = 0; i < e->getEmp().size(); i++) {
        if (cedula == e->getEmp()[i]->getCi()) {

        }

    }
}

int Menu::count_emp() {
    if (e->getEmp().size() == MAX_EMPLEADO)throw 2;
    if (e->getEmp().size() == 0)throw 3;

    return 1;
}

