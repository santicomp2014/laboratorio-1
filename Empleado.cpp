#include "Empleado.h"
#include "Menu.h"
#include "Jornalero.h"
#include "Fijo.h"

Empleado::Empleado() {
}

Empleado::~Empleado() {
}

void Empleado::setNombre(string n) {
    nombre=n;
}

void Empleado::setCi(string c) {
    ci=c;
}

void Empleado::setEdad(int e) {
    edad=e;
}

void Empleado::setValor_hora(Paga p) {
    valor_hora = p;
}

string Empleado::getNombre() {
    return nombre;
}

string Empleado::getCi() {
    return ci;
}

int Empleado::getEdad() {
    return edad;
}

Paga Empleado::getValor_hora() {
    return valor_hora;
}

Paga Empleado::get_sueldo_peso() {

}

Paga Empleado::get_sueldo_dolar() {

}
Empresa* Empleado::getEmpresa() {
    return empresa;
}

void Empleado::setEmpresa(Empresa* e) {
    this->empresa=e;
}
    
    