#ifndef EMPLEADO_H
#define	EMPLEADO_H
#include <stdio.h>
#include "Paga.h"
#define MAX_EMPLEADO 50
class Empresa;
class Empleado {
private:
    string nombre;
    string ci;
    int edad;
    Paga valor_hora;
    Empresa* empresa;
public:
    Empleado();
    virtual ~Empleado();
    virtual Paga get_sueldo_peso() ;
    virtual Paga get_sueldo_dolar() ;
    void setNombre(string);
    void setCi(string);
    void setEdad(int);
    void setValor_hora(Paga);
    string getNombre();
    string getCi();
    int getEdad();
    Paga getValor_hora();
    Empresa* getEmpresa();
    void setEmpresa(Empresa* );
};
#endif	/* EMPLEADO_H */

