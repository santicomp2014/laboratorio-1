#include "Fijo.h"

Fijo::Fijo() {
}

Fijo::Fijo(string nombre, string ci, int edad, Paga valor_hora,Empresa *e) {
    Empleado::setNombre(nombre);
    Empleado::setCi(ci);
    Empleado::setEdad(edad);
    Empleado::setValor_hora(valor_hora);
    Empleado::setEmpresa(e);
}


Fijo::~Fijo() {
}

Paga Fijo::get_sueldo_dolar() {
    return getValor_hora().a_dolar()*40;
}

Paga Fijo::get_sueldo_peso() {
    return getValor_hora().a_peso()*40;
}