#ifndef PAGA_H
#define	PAGA_H
#include <stdio.h>
#include "Extras.h"
#include "Cambio.h"
#include <iostream>
#include <math.h>

using namespace std;

class Paga {
private:
    double monto;
    Moneda moneda;
public:
    Paga();
    Paga(double, Moneda);
    virtual ~Paga();
    Paga a_dolar();
    Paga a_peso();
    void setMonto(double);
    double getMonto();
    void setMoneda(Moneda);
    Moneda getMoneda();
    Paga operator*(int horas);
    void operator+=(Paga p);
    Paga operator+(Paga p);
    friend ostream &operator<<(ostream &x, Paga p);
};

#endif	/* PAGA_H */

