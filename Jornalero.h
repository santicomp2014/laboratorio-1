#ifndef JORNALERO_H
#define	JORNALERO_H
#include "Empleado.h"
#include "Empresa.h"
class Jornalero:public Empleado {
private:
    int horas;
public:
    Jornalero();
    Jornalero(string nombre,string ci,int edad, Paga valor_hora,Empresa *,int horas);
    virtual ~Jornalero();
    void setHoras(int);
    int getHoras();
    Paga get_sueldo_dolar();
    Paga get_sueldo_peso();
};

#endif	/* JORNALERO_H */

