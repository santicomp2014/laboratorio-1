#ifndef CAMBIO_H
#define	CAMBIO_H
#define DOLAR 22.6
class Cambio {
private:

public:
    Cambio();
    virtual ~Cambio();
    static double a_peso(double);
    static double a_dolar(double);  
};

#endif	/* CAMBIO_H */

