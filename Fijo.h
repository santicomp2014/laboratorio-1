#ifndef FIJO_H
#define	FIJO_H

#include "Empleado.h"
#include "Empresa.h"
class Fijo:public Empleado {
private:
    

public:
    Fijo();
    Fijo(string nombre,string ci,int edad, Paga valor_hora,Empresa *e);
    virtual ~Fijo();
    Paga get_sueldo_dolar();
    Paga get_sueldo_peso();
};

#endif	/* FIJO_H */

